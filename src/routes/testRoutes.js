const express = require("express");
const testCtrlr = require("../controllers/testCtrlr");

const router = express.Router()

router.get("/api/success", testCtrlr.statusSuccess);
router.get("/api/fail", testCtrlr.statusFail);
router.post("/api/internal-error", testCtrlr.statusInternalError);

module.exports = router