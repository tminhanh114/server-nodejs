const express = require('express')
const morgan = require('morgan')
const cors = require('cors')
const router = require("./routes/testRoutes")

const app = express()
//middLware
app.use(cors()) 
app.use(morgan(':method :url :status :response-time ms - :date[iso]')); 
app.use(express.json()) 
app.use(express.urlencoded()); 

// app.get('/api/success', (req, res) => {
//   const time = new Date().toString();
//   const method = req.method;

//   res.status(200).json({ 
//     status: res.statusCode,
//     message: "Hello world", 
//     time: time, 
//     method: method, 
//     path: req.url 
//     });
// });

// app.get('/api/fail', (req, res) => {
//   const time = new Date().toString();
//   const method = req.method;

//   res.status(403).json({ 
//     status: res.statusCode ,
//     message: "Forbidden", 
//     time: time, 
//     method: method, 
//     path: req.url 
//     });
// });

// app.post('/api/internal-error', (req, res) => {
//   const time = new Date().toString();
//   const method = req.method;

//   res.status(500).json({
//     status: res.statusCode, 
//     message: "Cannot connect to database", 
//     time: time, 
//     method: method, 
//     path: req.url  
//     });
// });
app.use(router);

const port = process.env.POST || 3000;
app.listen(port,()=>{
    console.log(`Express is listening on port ${port}`)
})