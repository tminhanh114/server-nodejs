const testCtrlr = {
    statusSuccess: (req, res) => {
        const time = new Date().toString();
        const method = req.method;

        return res.status(200).json({
            status: res.statusCode,
            messagse: "Hello World",
            time: time,
            method: method,
            path: req.url
        })
    },

    statusFail: (req, res) => {
        const time = new Date().toString();
        const method = req.method;

        return res.status(403).json({
            status: res.statusCode,
            messagse: "You haven't permission access this feature",
            time: time,
            method: method,
            path: req.url
        })
    },

    statusInternalError: (req, res) => {
        const time = new Date().toString();
        const method = req.method;

        return res.status(500).json({
            status: res.statusCode,
            messagse: "Cannot connect to database",
            time: time,
            method: method,
            path: req.url
        })
    }
}

module.exports = testCtrlr;